# README #

Floor visualizers can be employed to create an illusion of space. 
They are extremely versatile, together with the ability to project effects, shadows and light .
This usually means that you can choose a projector which projects a ceiling picture or a black and white space, or create an area that is both spacious and clean. 
The technology https://floori.pl behind these visualizers is comparatively simple, but it takes skill to attain excellent results.







